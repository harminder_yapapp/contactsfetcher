contacts-fetcher-example
=======================

Example of fetching contacts using Content Providers

   minSdkVersion 10
   targetSdkVersion 25


    Step 1. Add the JitPack repository to your build file
    =====================================================

	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}


	Step 2. Add the dependency
	=====================================================

	dependencies {
	        compile 'org.bitbucket.harminder_yapapp:contactsfetcher:v1.0.13'
	}

	Step 3. implement  ContactFetcher.Result in your activity/fragment
	=====================================================

	public class MainActivity extends Activity implements  ContactFetcher.Result

	You will get this method

	@Override
    	public void resultContact(ArrayList<Contact> listContacts) {
    		/**any action you want to perform**/
    	}


	Step 4. Create object of ContactFetcher class with context & ContactFetcher.Result calback arguements.
    =====================================================

    ContactFetcher	contactFetcher	=	new ContactFetcher(Context,ContactFetcher.Result);

    Step 5. call fetchAll method. This method will return contacts in resultContact method.
    =====================================================

    For Marshamllow and above devices ask permission android.permission.READ_CONTACTS" before executing this step.


    contactFetcher.fetchAll();

    ----------------------------------------------------------------------------------------


    For fetching device phone number you need to call

    String phone = contactFetcher.getMyPhoneNo();

    Note: fetching phone no can only be done if sim structure allow you to do so.
     i.e it will return empty in that case





