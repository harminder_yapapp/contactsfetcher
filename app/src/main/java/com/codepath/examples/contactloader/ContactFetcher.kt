package com.codepath.examples.contactloader

import android.content.Context
import android.database.Cursor
import android.os.Handler
import android.os.Message
import android.provider.ContactsContract
import android.provider.ContactsContract.CommonDataKinds.Email
import android.provider.ContactsContract.CommonDataKinds.Phone
import android.support.v4.content.CursorLoader
import android.telephony.TelephonyManager
import android.util.Log

import java.util.ArrayList
import java.util.HashMap

class ContactFetcher(private val context: Context, callback: Result) {
     var isEnable=false
    internal val numberProjection = arrayOf(Phone.NUMBER, Phone.TYPE, Phone.CONTACT_ID)
    internal val emailProjection = arrayOf(Email.DATA, Email.TYPE, Email.CONTACT_ID)
    internal var listContacts = ArrayList<Contact>()
    internal var cursorLoader: CursorLoader?=null
    internal var phone: Cursor?=null
    internal var email: Cursor?=null
    private var cancelContactfetch = false
    internal var updateUIHandler: Handler = object : Handler() {

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            if (!cancelContactfetch) {
                callback.resultContact(listContacts)
            }
        }
    }



    /**
     * @return phone number
     */
    val myPhoneNo: String
        get() {
            val tm = context.applicationContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            return tm.line1Number
        }

    internal fun destroy() {
        cancelContactfetch = true
    }

    /**
     * used to fetch all contact details
     */
    fun fetchAll() {
        createCursor()

        Thread(Runnable {
            val c = cursorLoader?.loadInBackground()

            val contactsMap = HashMap<String, Contact>(c!!.count)

            if (c!!.moveToFirst()) {

                val idIndex = c.getColumnIndex(ContactsContract.Contacts._ID)
                val nameIndex = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)

                do {
                    if (cancelContactfetch)
                        break

                    val contactId = c.getString(idIndex)
                    val contactDisplayName = c.getString(nameIndex)

                    val path = "" /*= getphotopath(c, contactId);*/

                    val adressDetail = fetchAdressIfAny(contactId)

                    val contact = Contact(contactId, contactDisplayName, path, adressDetail)
                    contactsMap.put(contactId, contact)
                    listContacts.add(contact)
                } while (c.moveToNext())
            }

            c.close()


            matchContactNumbers(phone!!, contactsMap)
            matchContactEmails(email!!, contactsMap)

            // update ui call
            updateUIHandler.sendEmptyMessage(0)
        }).start()


    }

    private fun createCursor() {
        cursorLoader = CursorLoader(context,
                ContactsContract.Contacts.CONTENT_URI, null, null, null, null// the sort order (default)
        )// the columns to retrieve
        // the selection criteria (none)
        // the selection args (none)


        phone = CursorLoader(context,
                Phone.CONTENT_URI,
                numberProjection, null, null, null).loadInBackground()


        email = CursorLoader(context,
                Email.CONTENT_URI,
                emailProjection, null, null, null).loadInBackground()
    }

    /**
     * @param contactId
     * @return adress detail
     */
    private fun fetchAdressIfAny(contactId: String): String {

        //        String poBox = null;
        //        String street = null;
        //        String city = null;
        //        String state = null;
        //        String postalCode = null;
        //        String country = null;
        //        String type = null;
        var formattedAddress=""
        val addrWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?"
        val addrWhereParams = arrayOf(contactId, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
        val addrCur = context.contentResolver.query(ContactsContract.Data.CONTENT_URI, null, addrWhere, addrWhereParams, null)
        while (addrCur!!.moveToNext()) {
            //            poBox = addrCur.getString(
            //                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POBOX));
            //            street = addrCur.getString(
            //                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
            //            city = addrCur.getString(
            //                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
            //            state = addrCur.getString(
            //                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION));
            //            postalCode = addrCur.getString(
            //                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
            //            country = addrCur.getString(
            //                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
            //            type = addrCur.getString(
            //                    addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE));
            formattedAddress = addrCur.getString(addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS))
        }
        addrCur.close()

        if(isEnable)
        Log.e("formattedAddress", ":" + formattedAddress)

        return formattedAddress

    }


    fun log(isEnable:Boolean)
    {
        this.isEnable=isEnable
    }

    /**
     * matching name map with contactId and inserting phone no's.
     *
     * @param phone
     * @param contactsMap
     */
    fun matchContactNumbers(phone: Cursor, contactsMap: Map<String, Contact>) {
        // Get numbers


        if (phone.moveToFirst()) {
            val contactNumberColumnIndex = phone.getColumnIndex(Phone.NUMBER)
            val contactTypeColumnIndex = phone.getColumnIndex(Phone.TYPE)
            val contactIdColumnIndex = phone.getColumnIndex(Phone.CONTACT_ID)

            while (!phone.isAfterLast) {
                val number = phone.getString(contactNumberColumnIndex)
                val contactId = phone.getString(contactIdColumnIndex)
                val contact = contactsMap[contactId] ?: continue
                val type = phone.getInt(contactTypeColumnIndex)
                val customLabel = "Custom"
                val phoneType = ContactsContract.CommonDataKinds.Phone.getTypeLabel(context.resources, type, customLabel)
                contact.addNumber(number, phoneType.toString())
                phone.moveToNext()
            }
        }

        phone.close()
    }

    /**
     * matching name map with contactId and inserting email.
     *
     * @param email
     * @param contactsMap
     */
    fun matchContactEmails(email: Cursor, contactsMap: Map<String, Contact>) {
        // Get email


        if (email.moveToFirst()) {
            val contactEmailColumnIndex = email.getColumnIndex(Email.DATA)
            val contactTypeColumnIndex = email.getColumnIndex(Email.TYPE)
            val contactIdColumnsIndex = email.getColumnIndex(Email.CONTACT_ID)

            while (!email.isAfterLast) {
                val address = email.getString(contactEmailColumnIndex)
                val contactId = email.getString(contactIdColumnsIndex)
                val type = email.getInt(contactTypeColumnIndex)
                val customLabel = "Custom"
                val contact = contactsMap[contactId] ?: continue
                val emailType = ContactsContract.CommonDataKinds.Email.getTypeLabel(context.resources, type, customLabel)
                contact.addEmail(address, emailType.toString())
                email.moveToNext()
            }
        }

        email.close()
    }

    interface Result {
        fun resultContact(listContacts: ArrayList<Contact>)
    }


}
